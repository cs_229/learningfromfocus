from keras.models import load_model
from cnn import read_rgb_images, read_depth_images

# validation on NYU dataset


if __name__ == "__main__":
    
    model = load_model('nyu_wt_1.h5')
    x_test = read_rgb_images(1, 5)
    y_test = read_depth_images(1, 5)

    mse, mean_absolute = model.evaluate(x_test, y_test, batch_size=32, verbose=1)
    print('Test result: mse = %s, mean_abosolute_error = %s'%(mse, mean_absolute))
    

    
