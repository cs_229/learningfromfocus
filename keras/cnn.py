from keras.models import Sequential
from keras.layers.core import Flatten, Dense, Dropout
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.optimizers import SGD, Adam
import numpy as np
from scipy import misc
import json

# training on the NYU dataset (224x224, images), 11670 samples 

# read images
nImg = 11670 # total number of images we have
ordering = np.arange(nImg) + 1
np.random.seed(0) # for reproducibility
ordering = np.random.permutation(ordering) # first 10,000 images will be used as training, the remainder for testing


def read_rgb_images(idx_start, batch_size):
    pre = '../train_data/all_224_224/rgb_'
    post = '_224_224.jpg'
    data = np.zeros((batch_size, 224, 224, 3))
    for i in range(batch_size):
        filename = pre + str(ordering[i+idx_start]).zfill(5) + post
        img = misc.imread(filename)
        data[i, :, :, :] = img
        print filename

    data = data.reshape([-1, 224, 224, 3])

    return data


def read_depth_images(idx_start, batch_size):
    pre = '../train_data/all_224_224/depth_'
    post = '_24_24.jpg'
    data = np.zeros((batch_size, 24*24))
    for i in range(batch_size):
        filename = pre + str(ordering[i+idx_start]).zfill(5) + post
        img = misc.imread(filename)
        imgg = img.reshape([1,-1]) # flatten the image
        data[i,:] = imgg
        print filename

    return data




def cnn_test(weights_path=None):
    model = Sequential()

    model.add(Convolution2D(16, 3, 3, border_mode='same', activation='relu', input_shape=(224,224,3) )) 
    model.add(Convolution2D(16, 3, 3, border_mode='same', activation='relu' )) 
    model.add(MaxPooling2D((2,2), strides=(2,2), border_mode='same')) # output: 16 x 112 x 112

    model.add(Convolution2D(32, 3, 3, border_mode='same', activation='relu')) 
    model.add(Convolution2D(32, 3, 3, border_mode='same', activation='relu')) 
    model.add(MaxPooling2D((2,2), strides=(2,2), border_mode='same')) # output: 32 x 56 x 56

    model.add(Convolution2D(64, 3, 3, border_mode='same', activation='relu')) 
    model.add(MaxPooling2D((2,2), strides=(2,2), border_mode='same')) # output: 64 x 28 x 28
    
    model.add(Convolution2D(128, 3, 3, border_mode='same', activation='relu')) 
    model.add(MaxPooling2D((2,2), strides=(2,2), border_mode='same')) # output: 128 x 14 x 14

    model.add(Flatten())
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(576, activation='relu'))

    if weights_path:
        model.load_weights(weights_path)

    return model

if __name__ == "__main__":
    
    model = cnn_test()
    adam = Adam(lr=1e-4)
    model.compile(optimizer=adam, loss='mse', metrics=['mean_absolute_error'])
    

    print("Reading all images...")
    all_xs = read_rgb_images(0, 10000) # 1-10000 are training images
    all_ys = read_depth_images(0, 10000)

    print("Finished reading images. Starting training...")
    # model.train_on_batch(batch_xs, batch_ys)      
    hist = model.fit(all_xs, all_ys, batch_size = 32, nb_epoch=30)      

    print("Optimization Finished! Saving model weights...")
    model.save('cnn_model.h5')

    # output the training history, in json format
    f = open('output_cnn.txt', 'w') 
    json.dump(hist.history, f)
    f.close()

    print("Testing...")
    x_test = read_rgb_images(10000, nImg-10000) # the rest are testing images
    y_test = read_depth_images(10000, nImg-10000)
    mse, mean_absolute = model.evaluate(x_test, y_test, batch_size=32, verbose=1)
    print('Test result: mse = %s, mean_abosolute_error = %s'%(mse, mean_absolute))    
