from keras.models import Sequential
from keras.layers.core import Flatten, Dense, Dropout
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.optimizers import SGD, Adam
import numpy as np
from scipy import misc

# training on the old 200x270 images

# read images
nImg = 2048#7451
batch_size = 2048


def read_rgb_images(idx_start, batch_size):
    pre = '../train_data/all/rgb_'
    post = '_200_270.jpg'
    for i in range(batch_size):
        filename = pre + str(i+idx_start).zfill(5) + post
        img = misc.imread(filename)
        if i == 0:
            data = img
        else:
            data = np.vstack( (data, img) )
        # print filename

    data = data.reshape([-1, 200, 270, 3])
    # print data.shape
    return data


def read_depth_images(idx_start, batch_size):
    pre = '../train_data/all/depth_'
    post = '_23_31.jpg'
    for i in range(batch_size):
        filename = pre + str(i+idx_start).zfill(5) + post
        img = misc.imread(filename)
        imgg = img.reshape([1,-1]) # flatten the image
        if i == 0:
            data = imgg
        else:
            data = np.vstack( (data, imgg) )
        # print filename

    return data




def cnn_test(weights_path=None):
    model = Sequential()

    model.add(Convolution2D(16, 3, 3, border_mode='same', activation='relu', input_shape=(200,270,3) )) 
    model.add(MaxPooling2D((2,3), strides=(2,3), border_mode='same')) # output: 16 x 100 x 90

    model.add(Convolution2D(32, 3, 3, border_mode='same', activation='relu')) 
    model.add(MaxPooling2D((2,2), strides=(2,2), border_mode='same')) # output: 32 x 50 x 45

    model.add(Convolution2D(64, 3, 3, border_mode='same', activation='relu')) 
    model.add(MaxPooling2D((5,5), strides=(5,5), border_mode='same')) # output: 64 x 10 x 9
    

    model.add(Flatten())
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    #model.add(Dense(1024, activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Dense(713, activation='relu'))

    if weights_path:
        model.load_weights(weights_path)

    return model

if __name__ == "__main__":
    
    model = cnn_test()
    adam = Adam(lr=1e-4)
    model.compile(optimizer=adam, loss='mse', metrics=['mean_absolute_error'])
    
    total_batch = int(nImg/batch_size)
    for epoch in range(1):
        for i in range(total_batch):
            batch_xs = read_rgb_images(i*batch_size+1, batch_size) # image numbering starts from 1
            batch_ys = read_depth_images(i*batch_size+1, batch_size)

            # model.train_on_batch(batch_xs, batch_ys)      
            model.fit(batch_xs, batch_ys, batch_size = 32, nb_epoch=100)      

    print("Optimization Finished!")
    
