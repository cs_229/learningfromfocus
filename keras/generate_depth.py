import numpy as np
from scipy import misc
from keras.models import load_model

# generate predicted depth
# To use: 
# 1. for stack images, use IMG_PICK = [1,15,30,40,45,47,49,50,51,52]
# 2. for single image, use # IMG_PICK = [52]
# 3. change "ex_num" in main function, it will generate the depth estimation in predict/ folder

# The following two functions are copied from cnn_focus.py, however, without shuffling
IMG_PICK = [1,15,30,40,45,47,49,50,51,52] # number of images we pick in the focus stack
# IMG_PICK = [52] # for comparison, only use one single rgb image
NUM_IMG_PICK = len(IMG_PICK)

def read_rgb_images(idx_start, batch_size):
    pre = '../train_data/focus/ex_'
    data = np.zeros((batch_size, 224, 224, 3*NUM_IMG_PICK))
    for i in range(batch_size):
        filename_pre = pre + str(i+idx_start).zfill(5) + '/image_'
        for j in range(NUM_IMG_PICK): # choose from 52 images with different focus
            filename = filename_pre + str(IMG_PICK[j]).zfill(4) + '.jpg'
            img = misc.imread(filename)
            data[i, :, :, j:j+3] = img
            print filename
    return data
    

def read_depth_images(idx_start, batch_size):
    pre = '../train_data/focus/ex_'
    data = np.zeros((batch_size, 24*24))
    for i in range(batch_size):
        filename = pre + str(i+idx_start).zfill(5) + '/depthimage.jpg'
        img = misc.imread(filename)
        imgg = img.reshape([1,-1]) # flatten the image
        data[i,:] = imgg

    return data



if __name__ == "__main__":
    
    model = load_model('results/cnn_focus_wts_1.h5')
    
    for ex_num in range(244,3441):
        x = read_rgb_images(ex_num, 1) # make sure the IMG_PICK is correctly set

        depth = model.predict(x, batch_size=1, verbose=0)
        depth = depth.reshape([24,24])
        depth_int = np.around(depth) # round float to int
        depth_int = depth_int.astype(int)

        # depth_img = misc.toimage(depth_int, high=np.max(depth_int), low=np.min(depth_int), mode='I')
        depth_img = misc.toimage(depth_int, cmin=0, cmax=255, mode='I')
        filename = 'predict/depth_' + str(ex_num).zfill(5) + '.png'
        depth_img.save(filename)

    print("Finished generating estimated depth image!")
    

    
