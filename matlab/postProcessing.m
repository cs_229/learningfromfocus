%--------------------------------------------------------------------------
%
% File Name:      postProcessing.m
% Date Created:   2016/12/03
% Date Modified:  2016/12/03
%
% Author:         Eric Cristofalo
% Contact:        eric.cristofalo@gmail.com
%
% Description:    Script for post processing depth from focus and depth
%                 images for CS229 Final Project
%                 1) Undistorts Kinect Image
%                 2) Aligns Images
%
%                 Requirements: all images must have the same
%                 naming convention, i.e.
%                    {depth_image.jpg, im_0001.jpg, ... , im_0052.jpg}
%
% Inputs:
%
% Outputs:
%
%--------------------------------------------------------------------------

clean('Figures',0);

%% Initialization

addpath ../nyu_toolbox;

% Input Data Set Parameters
folderPath = '/Users/ericcristofalo/Dropbox/PhD/Shared/eric_experiment/CS_229/20161203_Data';
dataDigits = 5;
dataDigits = ['%0',num2str(dataDigits),'d'];
imageDigits = 4;
imageDigits = ['%0',num2str(imageDigits),'d'];
dataName = 'ex_';
depthImageName = 'depthimage.jpg';
imageName = 'image_';

% Output Data Set Parameters
folderPathOut = '/Users/ericcristofalo/Dropbox/PhD/Shared/eric_experiment/CS_229/20161203_Train';
imageDigitsOut = 2;
imageDigits = ['%0',num2str(imageDigits),'d'];
depthImageNameOut = 'depth.jpg';
imageNameOut = 'focus_';

% Data Used in Post Processing
% dataIndex = 0; % 0 for all data in folderPath
dataIndex = 1:3; % user specified indices of data

% User Inputs
displayImagesBool = 0;
filteringOption = 0; % 0: Eric's Filtering
% 1: NYU Toolbox Filtering
imDepthSizeOut = [24,24];
imSizeOut = [224,224];

% Replicate Data Options
replicateData = 1;
numReplicate = 2;
replicateInd = 0;


%% Process Entire Data Set

% Find All Folders in Data Directory if Data Not Specified
if dataIndex==0
   folderList = dir(folderPath);
   dataIndex = [];
   for i = 1:length(folderList)
      if ~strcmp(folderList(i).name(1),'.')
         ind = str2num(folderList(i).name(length(dataName)+1:end));
         dataIndex = [dataIndex,ind];
      end
   end
end

% Find All Files in Current Directory
fileList = dir([folderPath,'/',dataName,num2str(dataIndex(1),dataDigits)]);
imageList = cell(size(fileList,1)-2,1);
for i = 1:length(fileList)
   if ~strcmp(fileList(i).name(1),'.')
      imageList{i-2} = {fileList(i).name};
   end
end
numImages = length(imageList);
totalInd = numImages-1;

% Image Calibration
K_cam = [619.3779090692473, 0, 332.4387376815063;
   0, 619.1947556849789, 238.4406174277645;
   0, 0, 1];
d_cam = [0.129622820424235, -0.2468735716943058, -0.001409590607875395, 0.000365340269428765, 0.08368336387288254];
% K_depth = [365.402802,0,512/2;0,365.402802,424/2;0,0,1]; % Guess
K_depth = [365.402802,0,260.925507;0,365.402802,205.594604;0,0,1];
% d_depth = [-9.9897236553084481e-02,...
%             3.9065324602765344e-01,...
%             1.9290592870229277e-03,...
%            -1.9422022475975055e-03,...
%            -5.1031725053400578e-01]; % NYU
d_depth = [0.095575,...
   -0.277055,...
   0,...
   0,...
   0.097539];

% Read Data Examples
numData = length(dataIndex);
newDataInd = dataIndex(end);
for dataInd = dataIndex
   
   % Current Directory
   curFolder = [folderPath,'/',dataName,num2str(dataInd,dataDigits)];
   disp(['Processing directory: ',dataName,num2str(dataInd,dataDigits)]);
   
   % Create Output Directory
   outFolder = [folderPathOut,'/',dataName,num2str(dataInd,dataDigits)];
   if exist(outFolder,'file')~=7
      mkdir(outFolder)
   end
   
   % Replicate Data?
   if replicateData~=1
      numReplicate = 0; % do not replicate
   end
   
   % Replication Loop
   for repInd = 1:numReplicate+1

      % Initialize Random Data
      if replicateData==1 && repInd>1
         newDataInd = newDataInd+1;
         % Create Output Directory
         outFolderRep = [folderPathOut,'/',dataName,num2str(newDataInd,dataDigits)];
         if exist(outFolderRep,'file')~=7
            mkdir(outFolderRep)
         end
         % New Random Transformation
         eulerRand = deg2rad(rand(3,1)*10-5);
      end
      
      %--------------------------------------------------------------------------
      % Process Focal Stack
      imTotal = struct;
      imLoopInd = 0;
      for imInd = 2:numImages
         % Read Focal Image
         imRaw = imread([curFolder,'/',char(imageList{imInd})]);
         if replicateData==1 && repInd>1
            imRaw = imRectify(imRaw,K_cam,eulerRand);
         end
         %       imRaw = cv.undistort(imRaw, K_cam, d_cam);
         % Crop Focal Image
         imSize = [480,640];
         maxdF = 5;
         dF = round(maxdF/(52^2)*(imInd-1-1)^2); % inverse zooming crop factor
         d = (imSize(2)-imSize(1))/2;
         rowInd = (dF+1):(imSize(1)-dF);
         colInd = (d+1+dF):(imSize(2)-d-dF);
         imCrop = imRaw(rowInd,colInd,:);
         % Save Processed Focal Image
         imOut = imresize(imCrop,imSizeOut);
         if replicateData==1 && repInd>1
            imwrite(imOut,[outFolderRep,'/',char(imageList{imInd})]);
         else
            imwrite(imOut,[outFolder,'/',char(imageList{imInd})]);
         end
         %          figure(10); subimage(imOut);
      end
      
      %--------------------------------------------------------------------------
      % Read Depth Image
      imDepthRaw = imread([curFolder,'/',char(imageList{1})]);
      if replicateData==1 && repInd>1
         imDepthRaw = imRectify(imDepthRaw,K_depth,eulerRand);
      end
      % Transform Depth Image to Align with RGB Image
      imDepthCrop = imDepthRaw;
      %    imDepthCrop = cv.undistort(imDepthCrop, K_depth, d_depth);
      euler = deg2rad([-2;1;0]);
      imDepthCrop = imRectify(imDepthCrop,K_depth,euler);
      % Crop Depth Image
      imSize = [424,512];
      d = (imSize(2)-imSize(1))/2;
      imDepthCrop = imDepthCrop(:,(d+1):(imSize(2)-d));
      d = 70;
      range = (d+1):(imSize(1)-d);
      yOff = -20; % move crop window up with +yOff wrt rgb image
      imDepthCrop = imDepthCrop(range+yOff,range);
      % Filter Depth Image
      imDepthFilt = imDepthCrop;
      if filteringOption==0
         %       imDepthFilt = cv.blur(imDepthFilt,'KSize',[3,3]);
         imDepthFilt = cv.erode(imDepthFilt,'Element',[0,1,0;1,1,1;0,1,0]);
         imDepthFilt = cv.dilate(imDepthFilt,'Element',[0,1,0;1,1,1;0,1,0]);
         imDepthFilt = cv.dilate(imDepthFilt,'Element',[0,1,0;1,1,1;0,1,0]);
         imDepthFilt = cv.dilate(imDepthFilt,'Element',[0,1,0;1,1,1;0,1,0]);
         imDepthFilt = cv.blur(imDepthFilt,'KSize',[3,3]);
      elseif filteringOption==1
         imgDepthFilt = fill_depth_cross_bf(imCrop, double(imDepthFilt));
         % this filters and smooths the depth data
         % only works on Linux
      end
      % Resize Depth Filtered Image
      imDepthOut = imresize(imDepthFilt,imDepthSizeOut);
      % Fill in Missing Depth Data
      [rowInd,colInd] = find(imDepthOut==0);
      windowSize = 1;
      finishedCheck = 0;
      while (~isempty(rowInd)&&finishedCheck<2)
         rowDel = [];
         for i = 1:length(rowInd)
            rowRange = rowInd(i)-1:1:rowInd(i)+1;
            colRange = colInd(i)-1:1:colInd(i)+1;
            rowRange(rowRange<1)=[];
            rowRange(rowRange>imDepthSizeOut(1))=[];
            colRange(colRange<1)=[];
            colRange(colRange>imDepthSizeOut(2))=[];
            newVal = mean(mean(double(imDepthOut(rowRange,colRange))));
            if newVal~=0
               imDepthOut(rowInd(i),colInd(i)) = uint8(newVal);
               rowDel = [rowDel,i];
            end
         end
         rowInd(rowDel) = [];
         colInd(rowDel) = [];
         if size(rowDel)==0
            finishedCheck = finishedCheck+1;
         else
            finishedCheck = 0;
         end
      end
      % Save Processed Depth Image
      imDepthOut = uint8(imDepthOut);
      if replicateData==1 && repInd>1
         imwrite(imDepthOut,[outFolderRep,'/',char(imageList{1})]);
      else
         imwrite(imDepthOut,[outFolder,'/',char(imageList{1})]);
      end
      
      %--------------------------------------------------------------------------
      % Display Results
      if displayImagesBool==1
         figure(1);
         subplot(2,3,1); imshow(scale2RGB(double(imDepthRaw),[0,255])); title('Depth Raw');
         subplot(2,3,2); imshow(scale2RGB(double(imDepthFilt),[0,255])); title('Depth Filtered');
         subplot(2,3,3); imshow(scale2RGB(double(imDepthOut),[0,255])); title('Depth Train');
         subplot(2,3,4); imshow(imRaw); title('Image Raw');
         subplot(2,3,5); imshow(imCrop); title('Image Cropped');
         subplot(2,3,6); imshow(imOut); title('Image Train');
         impixelinfo;
         % Alignment Image
         imAlign = imresize(imDepthFilt,size(imCrop(:,:,1)));
         weight = 0.5;
         imAlign = uint8(weight*(imCrop)/4+(1-weight)*(scale2RGB(double(imAlign),[0,255])));
         figure(2);
         subimage(imAlign);
         title('Use this overlay to assess image alignment quality');
         impixelinfo;
      end
      
   end % end replication
   
end

disp('Post Processing Completed');

