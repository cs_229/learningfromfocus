%--------------------------------------------------------------------------
%
% File Name:      plotLearningCurves.m
% Date Created:   2016/12/10
% Date Modified:  2016/12/10
%
% Author:         Eric Cristofalo
% Contact:        eric.cristofalo@gmail.com
%
% Description:    Script for post processing training data
%
% Inputs:
%
% Outputs:
%
%--------------------------------------------------------------------------

clean('Figures',0);

% Data
dataName0 = '/Users/ericcristofalo/Dropbox/PhD/Shared/Eric&Zijian/2016_CS229/Results/log_files/output_nyu.txt';
data0 = loadjson(dataName0);
dataName1 = '/Users/ericcristofalo/Dropbox/PhD/Shared/Eric&Zijian/2016_CS229/Results/log_files/output_cnn_focos_single_1.txt';
data1 = loadjson(dataName1);
dataName2 = '/Users/ericcristofalo/Dropbox/PhD/Shared/Eric&Zijian/2016_CS229/Results/log_files/output_cnn_focus_1.txt';
data2 = loadjson(dataName2);
tSpan = 1:length(data1.loss);

% Plot Loss
figure(1);
hold on; box on;
plot(tSpan,data0.loss,'r','LineWidth',2);
plot(tSpan,data1.loss,'g','LineWidth',2);
plot(tSpan,data2.loss,'b','LineWidth',2);
xlabel('Epochs'); ylabel('Loss (pixels)');
legend('Single RGB Image - NYU','Single RGB Image','RGB Focal Stack');
axis([1,tSpan(end),0,7000]);
hold off;

% Plot Mean Absolute Error
figure(2);
hold on; box on;
plot(tSpan,data0.mean_absolute_error,'r','LineWidth',2);
plot(tSpan,data1.mean_absolute_error,'g','LineWidth',2);
plot(tSpan,data2.mean_absolute_error,'b','LineWidth',2);
xlabel('Epochs'); ylabel('Mean Squared Error (pixels^2)');
legend('Single RGB Image - NYU','Single RGB Image','RGB Focal Stack');
axis([1,tSpan(end),0,70]);
hold off;

% Plot Figures
return;
figureHandleRange = [1,2];
set(findall(figureHandleRange,'-property','FontSize'),'FontSize',14);
set(findall(figureHandleRange,'-property','FontName'),'FontName','Times')
axisFontSize = 14;
for i = figureHandleRange
   figure(i);
   pos = get(i,'Position');
   set(i,'Position',[pos(1:2),560,420]);
end
set(gca,'FontSize',axisFontSize)
extension = 'pdf';
saveFigures('FigureRange',figureHandleRange,...
            'Extension',extension);



