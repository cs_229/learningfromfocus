%--------------------------------------------------------------------------
%
% File Name:      postImages.m
% Date Created:   2016/12/10
% Date Modified:  2016/12/10
%
% Author:         Eric Cristofalo
% Contact:        eric.cristofalo@gmail.com
%
% Description:    Script for generating final prediction depth maps
%
% Inputs:
%
% Outputs:
%
%--------------------------------------------------------------------------

clean('Figures',0);

%% Process Output Images

% Output
folderPathOut = '/Users/ericcristofalo/Dropbox/PhD/Shared/Eric&Zijian/2016_CS229/Results/predict_nyu_out';

% Data
folderPath = '/Users/ericcristofalo/Dropbox/PhD/Shared/Eric&Zijian/2016_CS229/Results/predict_nyu';
dataDigits = 5;
dataDigits = ['%0',num2str(dataDigits),'d'];
totalNum = 3430;

% run('/Users/ericcristofalo/Dropbox/PhD/Shared/Eric&Zijian/2016_CS229/Results/nyu_test_indices.m');
% totalNum = 11669;

testSet = [2364, 1015, 2671, 3270,  291, 3410, 3625, 3139,  411, 1607, 1508,...
       2527, 1490, 1811, 2020,  515, 2593, 3075, 1755,  914, 2051, 3295,...
       1683,  561, 1873, 3478, 1816, 3323, 1141,  956,  164, 1671, 1701,...
       2395, 2118, 3177, 3557, 3618, 1525,  460, 2914,  952, 2845, 3480,...
       1543, 1858,  448, 1325,  797,  884,  750, 2878, 1017, 1855,  597,...
       1317, 3000,  359, 2482, 1039, 1174, 2522, 3219, 3561, 1552, 2712,...
        584,  658,  606, 2949, 1262, 1569, 1319,  481,  811,  292,  943,...
        743, 2774, 2891, 3010, 3360, 1155, 1628, 3096, 3301, 2514,  595,...
       2206,  161, 1122, 2809, 1926, 3176,  870,  963, 3368, 1342, 1230,...
       1226, 2687,  617,  165, 2165, 1051, 1989, 2676, 3529,  340, 2022,...
       2539, 2557, 3550, 1440, 3407, 2152,  820,  444, 1118, 2102,  871,...
        285,  978, 2769, 3638, 2150, 1357, 2824,  896,  567,  919, 1392,...
       2128, 2573,  520, 2399,  222, 1308,  147, 1788, 1064, 1833, 3197,...
       2862,  519,  334, 1572,  177, 2863, 1919,  169, 1411, 3603, 1403,...
       2268, 2188, 2248,  137, 1481, 3163, 1289, 1984,  905,  198, 2349,...
       1494, 1803, 2095,  778, 2291, 2704, 2092, 1123, 3558, 2861, 3127,...
        876, 2400, 1096, 1458,  200,  440, 3656, 1834, 1806, 2935, 1981,...
        741, 2978,  308, 2132,  818, 2606, 1576, 3008, 1183, 3508, 3515,...
        832, 2722, 1252, 1574, 3499,  634, 1456, 1729, 1173, 2271, 1651,...
       3094, 3513, 2614, 2786, 2760, 1033, 1761,  302, 2033, 1751, 2293,...
       1018, 3037,   68,   25,  217, 2871, 1921, 2794, 1164, 3432, 2170,...
        167, 3601,  208, 2333, 2100,  602, 1187, 1731, 1874,  536, 2346,...
        480, 1361, 2180, 2822, 1542, 3311,  939, 1907, 2732, 1630, 1480,...
       1152, 2945, 1924,  967, 1499,   26, 1145, 2208,  936, 3235, 1563,...
       2755,  854, 1656, 2948, 2657, 1031, 2198,  760, 3343, 3063, 2382,...
        181, 1283, 2509, 1445,  618, 3102, 1363, 2355, 1915, 3352, 1910,...
       3309,   95,  781,  648, 1315, 2345, 2547, 2503, 1219, 1406, 1589,...
       1209,  286, 2865, 1163, 1217, 2091, 1115,  318, 2829, 2070,  883,...
       3126,   87, 2837, 3119, 2764, 3141, 3124, 2841, 1446, 2621,  210,...
       3231, 2798, 1977, 2653, 1998, 2182, 2793,  942, 2157, 2436,  131,...
       1734, 2082, 3466,  965,  566, 2429, 1941, 1547, 2442,  202, 2976,...
       2034,  379,  984, 1645, 2446,  827, 1965, 1327, 1916,  267, 3280,...
       3178, 1483, 1710, 2228, 1616, 1340,  571, 1888,  384, 2968, 2778,...
       2850, 3013, 3310, 1695, 1178, 2035, 1215, 2927,  983, 1166, 2047,...
       3634, 1082, 3023,  785,  335, 2262,   85, 2692, 2967, 1600, 2670,...
       2457, 1931,   92, 3165,  441, 2838, 3202, 2350, 2116, 1356,  268,...
        899, 3632, 1477, 3125, 1652,  399, 2394,  284,  767, 3571, 1936,...
       3136, 1967, 3621, 2187,  813, 2087, 1218, 2941, 3542, 2569, 2734,...
        152, 2303,  725, 2602,  555,  650, 2756, 3348, 3371,  434, 2969,...
       3230,  670,  808, 3652, 3260, 2019,  100, 2909, 2507, 3514, 1882,...
       2057,  766,  987, 2174, 1951, 2836, 2131, 2904,  548, 1712, 2908,...
       2233, 2146, 2610, 3569,  716, 2373, 3479, 2659, 3555, 3153, 2658,...
       1839, 1789,  288, 1044, 3442, 1742,  774,  846, 3275, 1664, 2618,...
       2743];

totalSet = 1:totalNum;
trainSet = totalSet;
for i = 1:size(testSet,2)
   trainSet(trainSet==testSet(i)) = [];
end

% for i = 1:totalNum
%    i
%    fileName = [folderPath,'/depth_',num2str(i,dataDigits),'.png'];
%    try
%       im = imread(fileName);
%    catch
%       continue;
%    end
%    imProc = scale2RGB(double(im),[0,255]);
% %    subimage(imProc);
%    if any(testSet==i)
%       fileNameOut = [folderPathOut,'/depth_test_',num2str(i,dataDigits),'.png'];
%    else
%       fileNameOut = [folderPathOut,'/depth_train_',num2str(i,dataDigits),'.png'];
%    end
%    imwrite(imProc,fileNameOut);
% end

return
%% Compare Results

folderPath1 = '/Users/ericcristofalo/Dropbox/PhD/Shared/Eric&Zijian/2016_CS229/Results/predict_single';
folderPath2 = '/Users/ericcristofalo/Dropbox/PhD/Shared/Eric&Zijian/2016_CS229/Results/predict_focal';
folderPathData = '/Users/ericcristofalo/Dropbox/PhD/Research/2016_Depth_From_Focus/Training_Data/20161203_Train_Rep';
folderPathDataComp = '/Users/ericcristofalo/Dropbox/PhD/Shared/Eric&Zijian/2016_CS229/Results/predict_comp_our_data';

% for i = testSet
for i = 288;
   
   i
   
   fileName0 = [folderPathData,'/ex_',num2str(i,dataDigits),'/image_0052.jpg'];
   fileName1 = [folderPathData,'/ex_',num2str(i,dataDigits),'/depthimage.jpg'];
   fileName2 = [folderPath1,'/depth_',num2str(i,dataDigits),'.png'];
   fileName3 = [folderPath2,'/depth_',num2str(i,dataDigits),'.png'];
   
   try
      im0 = imread(fileName0);
      im1 = imread(fileName1);
      im1Disp = scale2RGB(double(im1),[0,255]);
      im2 = uint8(imread(fileName2));
      im2Disp = scale2RGB(double(im2),[0,255]);
      im3 = uint8(imread(fileName3));
      im3Disp = scale2RGB(double(im3),[0,255]);
   catch
      continue
   end
   figure(1); title(['Displaying Example ',num2str(i)]);
   subplot(2,3,1); subimage(im0); title('Raw Image');
   subplot(2,3,4); subimage(im1Disp); title('Ground Truth Image');
   subplot(2,3,2); subimage(im2Disp); title('Single RGB Prediction');
   subplot(2,3,5); subimage(im3Disp); title('Focal Stack Prediction');
   errorIm = abs(double(im2)-double(im1));
   subplot(2,3,3); subimage(scale2RGB(errorIm,[0,255]));
   title(['Single RGB Error: ',num2str(sum(errorIm(:)))]);
   errorIm = abs(double(im3)-double(im1));
   subplot(2,3,6); subimage(scale2RGB(errorIm,[0,255]));
   title(['Focal Stack Error: ',num2str(sum(errorIm(:)))]);
   
   fileNameOut = [folderPathDataComp,'/comp_',num2str(i,dataDigits),'.png'];
%    saveas(gcf,fileNameOut);

%    waitforbuttonpress;
   
end




















