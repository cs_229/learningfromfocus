% generate synchronized images for use in python
% adapted from demo_synched_projected_frames.m

clc
addpath ../nyu_toolbox

OUTPUT = 1; % 0 for visualization, 1 for generating files
OUTPUT_NUM_BIAS = 10922; % for putting all images together in the training database

% The directory where you extracted the raw dataset.
datasetDir = '~/Downloads/nyu_depth_dataset';

% The name of the scene to demo.
sceneName = 'bedroom_0015';

% The absolute directory of the 
sceneDir = sprintf('%s/%s', datasetDir, sceneName);

% Reads the list of frames.
frameList = get_synched_frames(sceneDir);

% Displays each pair of synchronized RGB and Depth frames.
if OUTPUT == 1
    step = 1;
else % visualization
    step = 15;
end
for ii = 1 : step : numel(frameList)
  imgRgb = imread([sceneDir '/' frameList(ii).rawRgbFilename]);
  imgDepthRaw = swapbytes(imread([sceneDir '/' frameList(ii).rawDepthFilename]));
  
  imgRgb_crop = imgRgb(11:470, 91:550, :); % make the image sqaure size
  
  % get the projected depth image.
  imgDepthProj = project_depth_map(imgDepthRaw, imgRgb);
  % smooth the depth data
  imgDepthFilled = fill_depth_cross_bf(imgRgb, double(imgDepthProj)); % this filters and smooths the depth data
  imgDepthFilled = imgDepthFilled(11:470, 91:550); % crop the image, drop 10 pixels on each edge (which is noisy)
  
  if OUTPUT == 0  % plot
      figure(1);
      subplot(1,3,1);
      imagesc(imgRgb_crop);
      axis off;
      axis equal;
      title('RGB');

      subplot(1,3,2);
      imagesc(imgDepthRaw);
      axis off;
      axis equal;
      title('Raw Depth');
      caxis([800 1100]);

      subplot(1,3,3);
      imagesc(imgDepthFilled);
      axis off;
      axis equal;
      title('Projected Depth');
  else % output images
      % output original size
%       img_name = sprintf('%04d.jpg', ii);
%       imwrite(imgRgb_crop, img_name);
%       depth_name = sprintf('%04d_depth.mat', ii);
%       save(depth_name, 'imgDepthFilled')
      
      % output the compressed size
      img_name = sprintf('gen_data/rgb_%05d_224_224.jpg', ii + OUTPUT_NUM_BIAS);
      imgRgb_crop_sm = imresize(imgRgb_crop, [224 224]);
      imwrite(imgRgb_crop_sm, img_name);
      
      imgDepthFilled_sm = imresize(imgDepthFilled, [24, 24]);    
      depth_gray = mat2gray(imgDepthFilled_sm);
      depth_name = sprintf('gen_data/depth_%05d_24_24.jpg', ii + OUTPUT_NUM_BIAS); % also save in csv file
      imwrite(depth_gray, depth_name);
  end
  
  pause(0.00001);
end
