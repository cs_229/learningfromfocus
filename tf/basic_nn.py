import tensorflow as tf
import numpy as np
from scipy import misc

# read images
nImg = 630
def read_rgb_images(idx_start, batch_size):
    pre = '../train_data/cafe_0001a_sm/'
    post = '_200_270.jpg'
    for i in range(batch_size):
        filename = pre + str(i+idx_start).zfill(4) + post
        img = misc.imread(filename)
        imgg = img.reshape([1,-1]) # flatten the image
        if i == 0:
            data = imgg
        else:
            data = np.vstack( (data, imgg) )
        # print filename

    return data


def read_depth_images(idx_start, batch_size):
    pre = '../train_data/cafe_0001a_sm/'
    post = '_depth_23_31.jpg'
    for i in range(batch_size):
        filename = pre + str(i+idx_start).zfill(4) + post
        img = misc.imread(filename)
        imgg = img.reshape([1,-1]) # flatten the image
        if i == 0:
            data = imgg
        else:
            data = np.vstack( (data, imgg) )
        # print filename

    return data


# Parameters
learning_rate = 0.001
training_epochs = 15
batch_size = 5
display_step = 1

# Network Parameters
n_hidden_1 = 256 # 1st layer number of features
n_hidden_2 = 256 # 2nd layer number of features
n_input = 54000*3 # MNIST data input (img shape: 200*270)
n_classes = 713 # outputs, 23*31

# tf Graph input
x = tf.placeholder("float", [None, n_input])
y = tf.placeholder("float", [None, n_classes])


# Create model
def multilayer_perceptron(x, weights, biases):
    # Hidden layer with RELU activation
    layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
    layer_1 = tf.nn.relu(layer_1)
    # Hidden layer with RELU activation
    layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
    layer_2 = tf.nn.relu(layer_2)
    # Output layer with linear activation
    out_layer = tf.matmul(layer_2, weights['out']) + biases['out']
    return out_layer

# Store layers weight & bias
weights = {
    'h1': tf.Variable(tf.random_normal([n_input, n_hidden_1])),
    'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_hidden_2, n_classes]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

# Construct model
pred = multilayer_perceptron(x, weights, biases)

# Define loss and optimizer
cost = tf.reduce_mean(tf.nn.l2_loss(pred-y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Initializing the variables
init = tf.initialize_all_variables()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)

    # Training cycle
    for epoch in range(training_epochs):
        avg_cost = 0.
        total_batch = int(nImg/batch_size)
        # Loop over all batches
        for i in range(total_batch):
            batch_x = read_rgb_images(i*batch_size+1, batch_size) # image numbering starts from 1
            batch_y = read_depth_images(i*batch_size+1, batch_size)
            # Run optimization op (backprop) and cost op (to get loss value)
            _, c = sess.run([optimizer, cost], feed_dict={x: batch_x,
                                                          y: batch_y})
            # Compute average loss
            avg_cost += c / total_batch
        # Display logs per epoch step
        if epoch % display_step == 0:
            print("Epoch:", '%04d' % (epoch+1), "cost=", \
                "{:.9f}".format(avg_cost))
    print("Optimization Finished!")

    # Test model
    #correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    # Calculate accuracy
    #accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    #print("Accuracy:", accuracy.eval({x: mnist.test.images, y: mnist.test.labels}))
