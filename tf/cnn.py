# View more python tutorial on my Youtube and Youku channel!!!

# Youtube video tutorial: https://www.youtube.com/channel/UCdyjiB5H8Pu7aDTNVXTTpcg
# Youku video tutorial: http://i.youku.com/pythontutorial

"""
Please note, this code is only for python 3+. If you are using python 2+, please modify the code accordingly.
"""
from __future__ import print_function
import numpy as np
import tensorflow as tf
from scipy import misc

learning_rate = 0.01 #1e-4

# read images
nImg = 7451
batch_size = 10


def read_rgb_images(idx_start, batch_size):
    pre = '../train_data/all/rgb_'
    post = '_200_270.jpg'
    for i in range(batch_size):
        filename = pre + str(i+idx_start).zfill(5) + post
        img = misc.imread(filename)
        if i == 0:
            data = img
        else:
            data = np.vstack( (data, img) )
        # print filename

    data = data.reshape([-1, 200, 270,3])
    # print data.shape
    return data


def read_depth_images(idx_start, batch_size):
    pre = '../train_data/all/depth_'
    post = '_23_31.jpg'
    for i in range(batch_size):
        filename = pre + str(i+idx_start).zfill(5) + post
        img = misc.imread(filename)
        imgg = img.reshape([1,-1]) # flatten the image
        if i == 0:
            data = imgg
        else:
            data = np.vstack( (data, imgg) )
        # print filename

    return data




def compute_accuracy(v_xs, v_ys):
    global prediction
    y_pre = sess.run(prediction, feed_dict={xs: v_xs, keep_prob: 1})
    correct_prediction = tf.equal(tf.argmax(y_pre,1), tf.argmax(v_ys,1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    result = sess.run(accuracy, feed_dict={xs: v_xs, ys: v_ys, keep_prob: 1})
    return result

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W):
    # stride [1, x_movement, y_movement, 1]
    # Must have strides[0] = strides[3] = 1
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
    # stride [1, x_movement, y_movement, 1]
    return tf.nn.max_pool(x, ksize=[1,2,3,1], strides=[1,2,3,1], padding='SAME')

# define placeholder for inputs to network
xs = tf.placeholder(tf.float32, [None, 200, 270, 3]) # 28x28
ys = tf.placeholder(tf.float32, [None, 713])
keep_prob = tf.placeholder(tf.float32)
# x_image = tf.reshape(xs, [-1, 28, 28, 1])
# print(x_image.shape)  # [n_samples, 28,28,1]

## conv1 layer ##
W_conv1 = weight_variable([5,5, 3,16]) # patch 5x5, in size 1, out size 32
b_conv1 = bias_variable([16])
h_conv1 = tf.nn.relu(conv2d(xs, W_conv1) + b_conv1) # output size 28x28x32
h_pool1 = max_pool_2x2(h_conv1)                                         # output size 14x14x32, 100x90x16

## conv2 layer ##
W_conv2 = weight_variable([5,5, 16, 32]) # patch 5x5, in size 32, out size 64
b_conv2 = bias_variable([32])
h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2) # output size 14x14x64
h_pool2 = max_pool_2x2(h_conv2)                                         # output size 7x7x64, 50x30x32

## fc1 layer ##
W_fc1 = weight_variable([50*30*32, 1024])
b_fc1 = bias_variable([1024])
# [n_samples, 7, 7, 64] ->> [n_samples, 7*7*64]
h_pool2_flat = tf.reshape(h_pool2, [-1, 50*30*32])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

## fc2 layer ##
W_fc2 = weight_variable([1024, 713])
b_fc2 = bias_variable([713])
# prediction = tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)
prediction = tf.matmul(h_fc1_drop, W_fc2) + b_fc2


# the error between prediction and real data
#cross_entropy = tf.reduce_mean(-tf.reduce_sum(ys * tf.log(prediction),
#                                              reduction_indices=[1]))       # loss
# cost = tf.reduce_mean(tf.nn.l2_loss(prediction-ys))
cost = tf.nn.l2_loss(prediction-ys)
train_step = tf.train.AdamOptimizer(learning_rate).minimize(cost)

sess = tf.Session()
# important step
sess.run(tf.initialize_all_variables())

for epoch in range(20):
    avg_cost = 0.
    total_batch = int(nImg/batch_size)

    for i in range(total_batch):
        batch_xs = read_rgb_images(i*batch_size+1, batch_size) # image numbering starts from 1
        batch_ys = read_depth_images(i*batch_size+1, batch_size)

        _, c = sess.run([train_step, cost], feed_dict={xs: batch_xs, ys: batch_ys, keep_prob: 0.5})
        avg_cost += c / total_batch

    print("Epoch:", '%04d' % (epoch), "cost=", \
                "{:.9f}".format(avg_cost))

print("Optimization Finished!")



    
