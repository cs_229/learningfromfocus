To use nyu_toolbox
1. extract the raw data to ~/Downloads/nyu_depth_dataset
2. Store the labeled .m file in ~/Downloads/nyu_depth_v2_labeled.mat
3. may need to run compile.m to compile the mex files
4. the most important script for us is: demo_synched_projected_frames.m

Packages in ROS:
- vision_general: Eric's generic vision nodes
