/*--------------------------------------------------------------------------
 
 File Name:         saveImage.cpp
 Date Created:      2015/07/13
 Date Modified:     2015/07/13
 
 Author:            Eric Cristofalo
 Contact:           eric.cristofalo@gmail.com
 
 Description:       Saves frame from video stream when given an save image boolean
 
 -------------------------------------------------------------------------*/

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <iostream>
#include <opencv2/features2d/features2d.hpp>
#include "opencv2/opencv.hpp"
#include <stdio.h>
#include "boost/filesystem.hpp"
#include <opencv2/core/core.hpp>
#include <fstream>
#include <string>
#include <std_msgs/Int8.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include "std_msgs/String.h"

using namespace std;
using namespace cv;

class saveImageClass{
    
    ros::NodeHandle nh_;
    image_transport::ImageTransport it_;
    image_transport::Subscriber image_sub_;
    cv_bridge::CvImagePtr cv_ptr_local;
    
    ros::Subscriber processImg_sub_;
    
    // Initialize
    bool processImg;
    int saveFrames;
    int imInd;
    
public:
    saveImageClass(int n) : it_(nh_) {
        
        // Subscribe to Image Topic
        image_sub_ = it_.subscribe("/image", 10, &saveImageClass::saveImageCallback, this);
        
        // Subscribe Process Image Boolean
        processImg_sub_ = nh_.subscribe("ProcessImg", 10,
                                         &saveImageClass::processImgCallback, this);

        saveFrames = n;
    }
    
    void processImgCallback(const std_msgs::Int8& msg)
    {
        // Grab Frame Now!
        processImg = true;
    }
    
    void saveImageCallback(const sensor_msgs::ImageConstPtr& msg) {
        
        // Read Image Message
        try {
            cv_ptr_local = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e) {
            ROS_ERROR_ONCE("Image CB :: cv_bridge exception: %s", e.what());
            return;
        }
        
        // Save Frame
        try {
            // Set ROS msg to CV Image
            Mat image = cv_bridge::toCvShare(msg, "bgr8")->image;
            
            // Display Image
            if (saveFrames==1 && processImg==true) {
                imshow("Image", image);
            
                // Create Output Directory if Desired Folder Does Not Exist
                string outputFolderPath = "/Users/ericcristofalo/Desktop/Images";
                ifstream ifile(outputFolderPath.c_str());
                if (!ifile) {
                    cout << "Creating output directory" << endl;
                    string command = "mkdir " + outputFolderPath;
                    system(command.c_str());
                }
                
                // Saving Images
                cout << "save image!" << endl;
                imInd++;
                ostringstream oss;
                oss << outputFolderPath << "/image_" << imInd << ".jpg";
                imwrite(oss.str(),image);
                
                // Set Process Image to False
                processImg = false;
                
            }
        }
        catch (cv_bridge::Exception& e) {
            ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
        }
    }
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "coordinator");
    ros::NodeHandle nh("~");
    int saveFramesParam;
    nh.param("saveFrames", saveFramesParam, 0);
    
    saveImageClass saveImageNode(saveFramesParam);
    
    while (ros::ok()){
        ros::spinOnce();
        if (waitKey(1)==27) break; // to display OpenCV image
    }
    ros::shutdown();
    
    cerr << "Exiting" << endl;
    return 0;
}
