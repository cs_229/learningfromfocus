/*--------------------------------------------------------------------------
 
 File Name:         publishImageFromFile.cpp
 Date Created:      2016/03/21
 Date Modified:     2016/03/22
 
 Author:            Eric Cristofalo
 Contact:           eric.cristofalo@gmail.com
 
 Description:       ROS node for publishing an image from file
 
 Inputs:            fileName:   string
                    showImage:  int
 
 -------------------------------------------------------------------------*/

#include <iostream>
#include <stdio.h>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Int8.h>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/features2d/features2d.hpp>

// Namespaces
using namespace std;                    // standard namespace
using namespace cv;                     // OpenCV namespace

int main(int argc, char** argv)
{
    ros::init(argc, argv, "image_publisher");
    ros::NodeHandle nh("~");
    image_transport::ImageTransport it(nh);
    image_transport::Publisher pub = it.advertise("/image", 1);
    
    // Initialization
    Mat image;
    String fileName = "/Users/ericcristofalo/Dropbox/PhD/Research/Computer_Vision/image.jpg";
    int showImage = 0;
    
    // Read ROS Parameter
    nh.param<string>("fileName", fileName, "/Users/ericcristofalo/Dropbox/PhD/Research/Computer_Vision/image.jpg");
    nh.param<int>("showImage", showImage, 0);
    
    // Read Image From File
    image = imread(fileName, CV_LOAD_IMAGE_COLOR);

    // Check for Invalid Input
    if(! image.data )
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
    
    // Create Window if Desire to Show Raw Stream
    if (showImage==1) {
        namedWindow("Raw Image");
    }
    
    // Set Image to ROS Message
    sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();
    
    ros::Rate loop_rate(1000);
    int count = 0; // image index counter
    while (nh.ok()) {
        
        // Update Image Index Counter
        count++;
    
        // Display Image
        if (showImage==1) {
            imshow("Raw Image From File", image);
        }
        waitKey(30);
        
        // Set Image to ROS Message
        sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();
        
        // Publish Message
        pub.publish(msg);
//        cout << "Image Sent: "+to_string(count) << endl;
        ros::spinOnce();
        loop_rate.sleep();
    }
}

