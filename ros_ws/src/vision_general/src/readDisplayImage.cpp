/*--------------------------------------------------------------------------
 
 File Name:         readImageFromNode.cpp
 Date Created:      2016/02/09
 Date Modified:     2016/02/09
 
 Author:            Eric Cristofalo
 Contact:           eric.cristofalo@gmail.com
 
 Description:       Read and display image on an image_transport topic for testing purposes
 
 -------------------------------------------------------------------------*/

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <iostream>
#include <opencv2/features2d/features2d.hpp>
#include "opencv2/opencv.hpp"
#include <stdio.h>
#include "boost/filesystem.hpp"
#include <opencv2/core/core.hpp>
#include <fstream>
#include <string>
#include <std_msgs/Int8.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include "std_msgs/String.h"

using namespace std;
using namespace cv;

class readImageClass{
    
    ros::NodeHandle nh_;
    image_transport::ImageTransport it_;
    image_transport::Subscriber image_sub_;
    cv_bridge::CvImagePtr cv_ptr_local;
    
    // Initialize
    int showImages;
    int showCenterLine;
    
public:
    readImageClass(int a, int b) : it_(nh_) {
        // Subscribe to Image Topic
        image_sub_ = it_.subscribe("/image", 10, &readImageClass::readImageCallback, this);
        // Set Input to Variable
        showImages = a;
        showCenterLine = b;
    }
    
    void addCenterLine(Mat& src, Mat& dst, int& length, int& thickness)
    {
        src.copyTo(dst); // dst color image
        int heightCenter = src.rows/2;   // scaled height of new smaller images
        int widthCenter = src.cols/2;    // scaled width of new smaler images
        
        // Color
        Vec3b color;
        color(0) = 0; color(1) = 0; color(2) = 255;
        // Verticle Line
        for (int i=heightCenter-length/2; i<heightCenter+length/2; i++) {
            for (int j = 0; j<thickness; j++) {
                dst.at<Vec3b>(i,widthCenter+j) = color;
                dst.at<Vec3b>(i,widthCenter-j) = color;
            }
        }
        // Horizontal Line
        for (int i=widthCenter-length/2; i<widthCenter+length/2; i++) {
            dst.at<Vec3b>(heightCenter,i) = color;
            for (int j = 0; j<thickness; j++) {
                dst.at<Vec3b>(heightCenter+j,i) = color;
                dst.at<Vec3b>(heightCenter-j,i) = color;
            }
        }
    }
    
    void readImageCallback(const sensor_msgs::ImageConstPtr& msg) {
        
        // Read Image Message
        try {
            cv_ptr_local = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e) {
            ROS_ERROR_ONCE("Image CB :: cv_bridge exception: %s", e.what());
            return;
        }
        
        // Display Frame
        try {
            if (showImages==1) {
                // Set ROS msg to CV Image
                Mat image = cv_bridge::toCvShare(msg, "bgr8")->image;
                // Display Center of Image
                if (showCenterLine==1){
                    int length = 100; // length of center line
                    int thickness = 1; // thickness of center line
                    addCenterLine(image, image, length, thickness);
                }
                // Display Image
                imshow("Image From Subscriber", image);
            }
        }
        catch (cv_bridge::Exception& e) {
            ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
        }
    }
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "coordinator");
    ros::NodeHandle nh("~");
    
    int showImagesParam;
    nh.param("showImages", showImagesParam, 1);
    int showCenterLineParam;
    nh.param("showCenterLine", showCenterLineParam, 0);
    
    readImageClass readImageNode(showImagesParam,showCenterLineParam);
    
    while (ros::ok()){
        ros::spinOnce();
        if (waitKey(1)==27) break; // to display OpenCV image
    }
    ros::shutdown();
    
    cerr << "Exiting" << endl;
    return 0;
}
