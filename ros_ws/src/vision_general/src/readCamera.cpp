/*--------------------------------------------------------------------------
 
 File Name:         readCamera.cpp
 Date Created:      2015/12/22
 Date Modified:     2016/02/08
 
 Author:            Eric Cristofalo
 Contact:           eric.cristofalo@gmail.com
 
 Description:       Reads and publishes image from USB camera
 
 -------------------------------------------------------------------------*/

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
	// ROS Initialization
	ros::init(argc, argv, "readCamera");
    ros::NodeHandle nh("~");
    image_transport::ImageTransport it(nh);
    image_transport::Publisher pub = it.advertise("/image", 1);
    
	// Initialize Camera
	Mat image;           // current image
	VideoCapture cap(1); // open the video camera no. 0 for built-in webcam
	if (!cap.isOpened()) // if not successful, exit program
	{
		cout << "No camera" << endl;
		return -1;
	}
    
	// Set Camera Properties
	// cap.set(CV_CAP_PROP_FRAME_WIDTH,320);
	// cap.set(CV_CAP_PROP_FRAME_HEIGHT,240);
    
	// Acquire Raw Frame
	if (!cap.read(image))	{
        cout << "No image" << endl;
	}
    
    // Set Image to ROS Message
    sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();
	
	// ROS loop
    ros::Rate loop_rate(1000);
    while (nh.ok()) {
       	
        // Acquire Raw Frame
       	if (!cap.read(image))	{
            cout << "No image" << endl;
            break;
       	}
       	else {
            
           // // Resize Image
           // float sizeScale = 0.8;
           // cv::Size dsize(image.cols*sizeScale,image.rows*sizeScale);
           // resize(image, image, dsize, 0, 0, CV_INTER_LINEAR);
            
           // // Display Image
           // if (waitKey(1)==27) return -1; // to display OpenCV image
           // imshow("Raw Image", image);
            
            // Set Image to ROS Message
           	sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();
            
           	// Publish Message
            pub.publish(msg);
            ros::spinOnce();
            loop_rate.sleep();
        }
    }
}

