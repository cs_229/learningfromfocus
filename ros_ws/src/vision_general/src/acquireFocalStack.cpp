/*--------------------------------------------------------------------------
 
 File Name:         acquireFocalStack.cpp
 Date Created:      2016/11/20
 Date Modified:     2016/11/20
 
 Author:            Eric Cristofalo
 Contact:           eric.cristofalo@gmail.com
 
 Description:       Acquires and Saves Images While Sweeping Through Focal Depth
 
 -------------------------------------------------------------------------*/

#include <iostream>
#include <cstdio>
#include <sstream>
#include <string>
#include <fstream>
#include <vector>
#include <dirent.h>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

/*function... might want it in some class?*/
int getdir (string dir, vector<string> &files)
{
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        files.push_back(string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}

class acquireFocalStackClass{

  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Subscriber image_sub2_;
  cv_bridge::CvImagePtr cv_ptr_local;
  cv_bridge::CvImagePtr cv_ptr_local2;

    // Initialize
  int saveFrames;
  int imInd;
  int focusValue;
  int imageDigits, folderDigits;
  string fileLocation;
  bool acquireStack, finishedCollectingData;
  string currentFilePath;

public:
  acquireFocalStackClass(int a, string b) : it_(nh_) {

    // Subscribe to Image Topic
    image_sub_ = it_.subscribe("/image", 10, &acquireFocalStackClass::acquireFocalStackCallback, this);

    // Subscribe to Image Topic
    image_sub2_ = it_.subscribe("/kinect2/sd/image_depth", 10, &acquireFocalStackClass::acquireDepthCallback, this);

    // Initialize Variables
    saveFrames = a;
    fileLocation = b;
    focusValue = 255;
    imageDigits = 4;
    folderDigits = 5;
    acquireStack = false;
    finishedCollectingData = true;
    imInd = 0;

    // Initialize Focus
    string focusCommand = "uvcdynctrl -d video1 -s 'Focus, auto' 0";
    system(focusCommand.c_str());
    focusCommand = "uvcdynctrl -d video1 -s 'Focus (absolute)' " + to_string(focusValue);
    system(focusCommand.c_str());

    // Acquire Data Image
    Mat acquireDataImage = Mat::ones(1000,300,CV_8UC1)*255;
    imshow("Acquire Data: Press 'Enter'",acquireDataImage);

    // Check for Desired Image Directory
    ifstream ifile(fileLocation.c_str());
    if (!ifile) {
      cout << "Creating output directory" << endl;
      string command = "mkdir " + fileLocation;
      system(command.c_str());
    }

  }

  void acquireDepthCallback(const sensor_msgs::ImageConstPtr& msg) {


    cv_bridge::CvImagePtr depthImg = cv_bridge::toCvCopy(msg , sensor_msgs::image_encodings::TYPE_32FC1);
    // Mat image = cv_bridge::toCvShare(msg, "mono8")->image;
    // cout << image << endl;
    //Normalize the pixel value
    Mat outImg;
    cv::normalize(depthImg->image, outImg, 1, 0, cv::NORM_MINMAX);
    outImg.convertTo(outImg, CV_8UC3, 255.0);     
    imshow("depth",outImg);

    if (acquireStack==true){
      ostringstream imagePath;
      imagePath << currentFilePath << "/depthimage.jpg";
      imwrite(imagePath.str(),outImg);
      cout << "Writing depth image: " + imagePath.str() << endl;
    }
  

    // // Read Image Message
    // try {
    //   cv_ptr_local2 = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_16UC1);
    // }
    // catch (cv_bridge::Exception& e) {
    //   ROS_ERROR_ONCE("Image CB :: cv_bridge exception: %s", e.what());
    //   return;
    // }

    // // Set ROS msg to CV Image
    // try {
    //   Mat image = cv_bridge::toCvShare(msg, "mono16")->image;
    //   // Display Image
    //   // imshow("Raw Depth Image", image);
    // }
    // catch (cv_bridge::Exception& e) {
    //   ROS_ERROR("Could not convert from '%s' to 'mono16'.", msg->encoding.c_str());
    // }
  }

  // void depthCb(const sensor_msgs::ImageConstPtr& msg)
  // {
  //   cv_bridge::CvImageConstPtr cv_ptr;
  //   try
  //   {
  //     cv_ptr = cv_bridge::toCvShare(msg, enc::TYPE_16UC1);
  //   }
  //   catch (cv_bridge::Exception& e)
  //   {
  //     ROS_ERROR("cv_bridge exception: %s", e.what());
  //     return;
  //   }

  //    ROS_INFO("Applying mask to depth image");
  //  Mat m=Mat(cv_ptr->image.size(), cv_ptr->image.type());

  //  cvtColor(dst,m,CV_RGB2GRAY); 
  //  Mat result=Mat(cv_ptr->image.size(), cv_ptr->image.type());
  //  cv_ptr->image.copyTo(result, m); 

  //   cv::imshow("depth_filtered", result);
  //   cv::imshow("depth_image", cv_ptr->image);
  //   cv::waitKey(1);

  // }

  void acquireFocalStackCallback(const sensor_msgs::ImageConstPtr& msg) {

    // Read Image Message
    try {
      cv_ptr_local = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e) {
      ROS_ERROR_ONCE("Image CB :: cv_bridge exception: %s", e.what());
      return;
    }

    // Save Frame
    try {
      // Set ROS msg to CV Image
      Mat image = cv_bridge::toCvShare(msg, "bgr8")->image;

      // Check Input Image for Acquiring Data
      if (waitKey(1)==13 && finishedCollectingData==true) {
        acquireStack = true;

        // Check Directory for Example Folders
        string dir = fileLocation;
        vector<string> files = vector<string>();
        getdir(dir,files);
        int curDirectoryIndex = 0;
        for (unsigned int i = 0;i < files.size();i++) { 
          string test = files[i];
          if (test.length()==8) {
            string curString (files[i],3,5);
            int curFolderIndex = stoi(curString);
            if (curFolderIndex>curDirectoryIndex) {
              curDirectoryIndex = curFolderIndex;
            }
          }
        }

        // Create Individual Example Folder
        string outputFolderPath = fileLocation;
        ostringstream folderNum;
        folderNum << setw(folderDigits) << setfill('0') << curDirectoryIndex+1;
        ostringstream oss;
        oss << outputFolderPath << "/ex_" << folderNum.str();
        ifstream ifile(oss.str());
        if (!ifile) {
          string command = "mkdir " + oss.str();
          system(command.c_str());
        }
        currentFilePath = oss.str();

      }

      // Display Image
      if (saveFrames==1 && acquireStack==true) {
        finishedCollectingData = false;

        // Saving Images
        imInd++;
        ostringstream imNum;
        imNum << setw(imageDigits) << setfill('0') << imInd;
        ostringstream imagePath;
        imagePath << currentFilePath << "/image_" << imNum.str() << ".jpg";
        imwrite(imagePath.str(),image);
        cout << "Writing image: " + imagePath.str() << endl;
        // Change Desired Focus Value
        string focusCommand = "uvcdynctrl -d video1 -s 'Focus (absolute)' " + to_string(focusValue);
        system(focusCommand.c_str());
        focusValue = focusValue-5;

        // Finished Collecting Data
        if (focusValue<0) {
          finishedCollectingData = true;
          acquireStack = false;
          imInd = 0;
          focusValue = 255;
          string focusCommand = "uvcdynctrl -d video1 -s 'Focus (absolute)' " + to_string(focusValue);
          system(focusCommand.c_str());
        }

      }
    }
    catch (cv_bridge::Exception& e) {
      ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }
  }
};

int main(int argc, char **argv)
{
  ros::init(argc, argv, "coordinator");
  ros::NodeHandle nh("~");
  int saveFramesParam;
  nh.param<int>("saveFrames", saveFramesParam, 0);
  string fileLocationParam;
  nh.param<string>("fileLocation", fileLocationParam, "/home/ericcristofalo/Desktop/Images");

  acquireFocalStackClass acquireFocalStackNode(saveFramesParam, fileLocationParam);

  while (ros::ok()){
    ros::spinOnce();
        if (waitKey(1)==27) break; // to display OpenCV image
      }
      ros::shutdown();

      cerr << "Exiting" << endl;
      return 0;
    }
