This folder contains pre-processed training data

=== cafe_0001a_sm ===
From NYU Depth V2 dataset
rgb is resized to 200 x 270
depth is resized to 23 x 31

=== all ===
From NYU Depth V2 dataset
rgb is resized to 200 x 270
depth is resized to 23 x 31

1-630: cafe_0001a
631-1413: cafe_0001c
1413-1529: bedroom_0001
1530-3801: bedroom_0003
3802-5174: bedroom_0004
5175-6032: bedroom_0005
6033-7451: bedroom_0006


=== all_224_224 ===
From NYU Depth V2 dataset
rescale images to 224x224 in order to fit the VGG model, depth is rescaled to 24x24
1 - 1425: bedroom_0007
1426 - 1541: bedroom_0008
1542 - 1674: bedroom_0009
1675 - 2570: bedroom_0010
2571 - 2686: bedroom_0001
2687 - 4958: bedroom_0003
4959 - 6331: bedroom_0004
6332 - 7189: bedroom_0005
7190 - 8608: bedroom_0006
8609 - 9030: bedroom_0011
9031 - 10409: bedrrom_0013
10410 - 10922: bedroom_0014
10923 - 11671: bedroom_0015



=== focus ===
Our own dataset, here are only some sample images. The entire dataset is too large and not stored in Git.
For each take, instead of 1 rgb image, we have a stack of images taken with different focal lengths.








